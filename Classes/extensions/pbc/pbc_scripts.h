//
//  pbc_scripts.h
//  mk
//
//  Created by Terran on 15/11/26.
//
//

#ifndef pbc_scripts_h
#define pbc_scripts_h

#include <stdio.h>

#if __cplusplus
extern "C" {
#endif
    
#include "lua.h"
void luaopen_pbc_scripts(lua_State* L);
    
#if __cplusplus
}
#endif



#endif /* pbc_scripts_h */
