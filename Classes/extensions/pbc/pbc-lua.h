//
//  pbc-lua.h
//  mk
//
//  Created by Terran on 15/11/25.
//
//

#ifndef pbc_lua_h
#define pbc_lua_h

#if __cplusplus
extern "C" {
#endif
    
    #include "lua.h"
    int luaopen_protobuf_c(lua_State* L);
    
#if __cplusplus
}
#endif


#endif /* pbc_lua_h */
