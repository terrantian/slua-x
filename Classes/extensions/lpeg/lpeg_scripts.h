//
//  lpeg_scripts.h
//  mk
//
//  Created by Terran on 15/12/3.
//
//

#ifndef lpeg_scripts_h
#define lpeg_scripts_h

#include <stdio.h>


#if __cplusplus
extern "C" {
#endif
    
#include "lua.h"
    void luaopen_lpeg_scripts(lua_State* L);
    
#if __cplusplus
}
#endif


#endif /* lpeg_scripts_h */
