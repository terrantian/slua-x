LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := libslua-x_shared
LOCAL_MODULE_FILENAME := libslua-x

#LOCAL_ARM_MODE := arm

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes/lua
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/extensions

LOCAL_CPPFLAGS := -03 -ffast-math
LOCAL_SRC_FILES :=  ./../../Classes/extensions/amf3/amf3.c \
					./../../Classes/extensions/amf3/amf3_decode.c \
					./../../Classes/extensions/amf3/amf3_encode.c \
					./../../Classes/extensions/cjson/fpconv.c \
					./../../Classes/extensions/cjson/lua_cjson.c \
					./../../Classes/extensions/cjson/strbuf.c \
					./../../Classes/extensions/lpeg/lpcap.c \
					./../../Classes/extensions/lpeg/lpcode.c \
					./../../Classes/extensions/lpeg/lpeg_scripts.c \
					./../../Classes/extensions/lpeg/lpprint.c \
					./../../Classes/extensions/lpeg/lptree.c \
					./../../Classes/extensions/lpeg/lpvm.c \
					./../../Classes/extensions/luasocket/auxiliar.c \
					./../../Classes/extensions/luasocket/buffer.c \
					./../../Classes/extensions/luasocket/except.c \
					./../../Classes/extensions/luasocket/inet.c \
					./../../Classes/extensions/luasocket/io.c \
					./../../Classes/extensions/luasocket/luasocket.c \
					./../../Classes/extensions/luasocket/luasocket_scripts.c \
					./../../Classes/extensions/luasocket/mime.c \
					./../../Classes/extensions/luasocket/options.c \
					./../../Classes/extensions/luasocket/select.c \
					./../../Classes/extensions/luasocket/serial.c \
					./../../Classes/extensions/luasocket/tcp.c \
					./../../Classes/extensions/luasocket/timeout.c \
					./../../Classes/extensions/luasocket/udp.c \
					./../../Classes/extensions/luasocket/unix.c \
					./../../Classes/extensions/luasocket/usocket.c \
					./../../Classes/extensions/pbc/alloc.c \
					./../../Classes/extensions/pbc/array.c \
					./../../Classes/extensions/pbc/bootstrap.c \
					./../../Classes/extensions/pbc/context.c \
					./../../Classes/extensions/pbc/decode.c \
					./../../Classes/extensions/pbc/map.c \
					./../../Classes/extensions/pbc/pattern.c \
					./../../Classes/extensions/pbc/pbc-lua.c \
					./../../Classes/extensions/pbc/pbc_scripts.c \
					./../../Classes/extensions/pbc/proto.c \
					./../../Classes/extensions/pbc/register.c \
					./../../Classes/extensions/pbc/rmessage.c \
					./../../Classes/extensions/pbc/stringpool.c \
					./../../Classes/extensions/pbc/varint.c \
					./../../Classes/extensions/pbc/wmessage.c \
					./../../Classes/lua/lapi.c \
					./../../Classes/lua/lauxlib.c \
					./../../Classes/lua/lbaselib.c \
					./../../Classes/lua/lcode.c \
					./../../Classes/lua/ldblib.c \
					./../../Classes/lua/ldebug.c \
					./../../Classes/lua/ldo.c \
					./../../Classes/lua/ldump.c \
					./../../Classes/lua/lfunc.c \
					./../../Classes/lua/lgc.c \
					./../../Classes/lua/linit.c \
					./../../Classes/lua/liolib.c \
					./../../Classes/lua/llex.c \
					./../../Classes/lua/lmathlib.c \
					./../../Classes/lua/lmem.c \
					./../../Classes/lua/loadlib.c \
					./../../Classes/lua/lobject.c \
					./../../Classes/lua/lopcodes.c \
					./../../Classes/lua/loslib.c \
					./../../Classes/lua/lparser.c \
					./../../Classes/lua/lstate.c \
					./../../Classes/lua/lstring.c \
					./../../Classes/lua/lstrlib.c \
					./../../Classes/lua/ltable.c \
					./../../Classes/lua/ltablib.c \
					./../../Classes/lua/ltm.c \
					./../../Classes/lua/lua.c \
					./../../Classes/lua/lundump.c \
					./../../Classes/lua/lvm.c \
					./../../Classes/lua/lzio.c \
					./../../Classes/lua/print.c \
					./../../Classes/slua.c 

include $(BUILD_SHARED_LIBRARY)
